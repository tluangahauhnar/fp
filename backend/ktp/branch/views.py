from django.shortcuts import render

from rest_framework import generics
from .serializers import KTPBranchSerializer

from .models import KTPBranch



class KTPBranchListAPIView(generics.ListAPIView):
    queryset = KTPBranch.objects.all()
    serializer_class= KTPBranchSerializer

class KTPBranchListCreateAPIView(generics.ListCreateAPIView):
    queryset = KTPBranch.objects.all()
    serializer_class = KTPBranchSerializer

class KTPBranchRetrieveUpdateDestroyAPIView(generics.RetrieveUpdateDestroyAPIView):
    queryset = KTPBranch.objects.all()
    serializer_class= KTPBranchSerializer



class KTPBranchRetrieveDestroyAPIView(generics.RetrieveDestroyAPIView):
    queryset = KTPBranch.objects.all()
    serializer_class= KTPBranchSerializer
    lookup_field='id'

class KTPBranchRetrieveUpdateAPIView(generics.RetrieveUpdateAPIView):
    queryset = KTPBranch.objects.all()
    serializer_class= KTPBranchSerializer
    lookup_field='id'