from rest_framework import serializers
from .models import KTPBranch


class KTPBranchSerializer(serializers.ModelSerializer):

    class Meta:
        model = KTPBranch
        fields = (
            'name',
            'date_of_establishment'
        )
