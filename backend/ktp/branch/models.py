from django.db import models

# Create your models here.


class KTPBranch(models.Model):
    """Model definition for KTPBranch."""
    name = models.CharField(max_length=50)
    date_of_establishment = models.DateField( auto_now=False, auto_now_add=False)

    class Meta:
        """Meta definition for KTPBranch."""

        verbose_name = 'KTPBranch'
        verbose_name_plural = 'KTPBranchs'

    def __str__(self):
        return self.name
