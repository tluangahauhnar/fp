from django.urls import path, include
from rest_framework import routers
from . import views

#router = routers.DefaultRouter()

#router.register('create/', )


urlpatterns = [
    path('',views.KTPBranchListAPIView.as_view(), name='branch-list'),
    path('updatedestroy/', views.KTPBranchRetrieveUpdateDestroyAPIView.as_view(), name='branchktp'),
    path('add/',views.KTPBranchListCreateAPIView.as_view(),name='add-branch'),
    path('remove/<int:id>', views.KTPBranchRetrieveDestroyAPIView.as_view(), name='remove-branch'),
    path('edit/<int:id>',views.KTPBranchRetrieveUpdateAPIView.as_view(),name='update-branch')
]
