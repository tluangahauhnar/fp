from django.db import models
from ktp.group.models import Group
from ktp.branch.models import KTPBranch
# Create your models here.
class Member(models.Model):
    """Model definition for Member."""
    hming = models.CharField(max_length=50)
    pa_nu_hming = models.CharField(max_length=50)
    group = models.ForeignKey(
        'group.Group',
        on_delete=models.CASCADE,
        related_name='members'
    )
    branch = models.ForeignKey(
        'branch.KTPBranch',
        on_delete=models.CASCADE,
        related_name='members'
    )
    
    is_active = models.BooleanField(default=True)

    class Meta:
        """Meta definition for Member."""

        verbose_name = 'Member'
        verbose_name_plural = 'Members'

    def __str__(self):
        return self.hming