from django.urls import path
from .views import MemberListAPIView, MemberListCreateAPIView, MemberRetrieveUpdateDestroyAPIView
from .views import MemberRetrieveUpdateAPIView

urlpatterns = [
    path('', MemberListAPIView.as_view(), name='member-list'),
    path('add/', MemberListCreateAPIView.as_view(), name='add-member'),
    path('update/<int:id>', MemberRetrieveUpdateAPIView.as_view(),
         name='member-update'),
    path('remove/', MemberRetrieveUpdateDestroyAPIView.as_view(),
         name='member-update'),
    path('bulkupdate/', MemberRetrieveUpdateAPIView.as_view(),
         name='member-bulkupdate')
]
