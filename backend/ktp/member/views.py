
from rest_framework import generics
from .models import Member
from .serializers import MemberSerializer


class MemberListAPIView(generics.ListAPIView):
    queryset = Member.objects.all()
    serializer_class = MemberSerializer
    


class MemberListCreateAPIView (generics.ListCreateAPIView):
    queryset = Member.objects.all()
    serializer_class = MemberSerializer



class MemberRetrieveUpdateDestroyAPIView (generics.RetrieveUpdateDestroyAPIView):
    queryset = Member.objects.all()
    serializer_class = MemberSerializer
    lookup_field = 'id'


class MemberRetrieveUpdateAPIView (generics.RetrieveUpdateAPIView):
    queryset = Member.objects.all()
    serializer_class = MemberSerializer(many=True)
    lookup_field = 'id'
