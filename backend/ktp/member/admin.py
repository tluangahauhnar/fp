from django.contrib import admin
from import_export.admin import ImportExportModelAdmin
from .models import Member
# Register your models here.

@admin.register(Member)
class ViewAdmin(ImportExportModelAdmin):
    pass