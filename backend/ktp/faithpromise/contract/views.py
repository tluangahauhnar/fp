from rest_framework import generics
from .models import Contract
from .serializers import ContractSerializer
from rest_framework_bulk import (
        ListBulkCreateUpdateDestroyAPIView,
)


class ContractListAPIView (generics.ListAPIView):
    queryset = Contract.objects.all()
    serializer_class = ContractSerializer


class ContractCreateAPIView (generics.CreateAPIView):
    queryset = Contract.objects.all()
    serializer_class = ContractSerializer


class ContractRetrieveUpdateAPIView (generics.RetrieveUpdateAPIView):
    queryset = Contract.objects.all()
    serializer_class = ContractSerializer(many=True)
    lookup_field = 'id'


class ContractRetrieveDestroyAPIView(generics.RetrieveDestroyAPIView):
    queryset = Contract.objects.all()
    serializer_class = ContractSerializer
    lookup_field = 'id'

class ContractListBulkCreateUpdateDestroyAPIView(ListBulkCreateUpdateDestroyAPIView):
    queryset = Contract.objects.all()
    serializer_class = ContractSerializer    
