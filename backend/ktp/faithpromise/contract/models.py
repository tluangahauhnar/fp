from django.db import models
from ktp.group.models import Incharge
from ktp.member.models import Member
from ktp.faithpromise.session.models import Session, SessionMonth

# Create your models here.

class Contract(models.Model):
    """Model definition for FP."""

    member = models.ForeignKey(
        'member.Member',
        on_delete=models.CASCADE,
        related_name='contracts')
    session = models.ForeignKey(
        'session.Session',
        on_delete=models.CASCADE,
        related_name='contracts'

    )
    incharge = models.ForeignKey(
        'group.Incharge',
        on_delete=models.CASCADE,
        related_name='contracts'
    )
    start_month_contract = models.DateField()
    end_month_contract = models.DateField()
    monthly_contract_amount = models.IntegerField()
    total_contract_amount = models.IntegerField()
    contract_remarks = models.TextField()

    class Meta:
        """Meta definition for FP."""

        verbose_name = 'Contract'
        verbose_name_plural = 'Contracts'

    def __str__(self):
        """Unicode representation of FP."""
        pass


class Transaction(Contract):
    key = models.CharField(max_length=50)
    amount = models.IntegerField()
    session_month = models.ForeignKey(
        'session.SessionMonth',
        on_delete=models.CASCADE,
        related_name='transactions'

    )
    
    main= models.ForeignKey(
        'Main',
        on_delete=models.CASCADE,
        related_name='transactions')

    class Meta:
        """Meta definition for Transaction."""

        verbose_name = 'Transaction'
        verbose_name_plural = 'transactions'

    def __str__(self):
        """Unicode representation of Transaction."""
        return self.name

class Main(models.Model):
    """Model definition for Main."""

    total_contribution = models.IntegerField()  # total contribution of a person

    class Meta:
        """Meta definition for Main."""

        verbose_name = 'Main'
        verbose_name_plural = 'Mains'

    def __str__(self):
        """Unicode representation of Main."""
        pass


