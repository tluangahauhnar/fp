# Generated by Django 2.1.2 on 2018-11-04 00:34

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('group', '0001_initial'),
        ('session', '0001_initial'),
        ('member', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Contract',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('start_month_contract', models.DateField()),
                ('end_month_contract', models.DateField()),
                ('monthly_contract_amount', models.IntegerField()),
                ('total_contract_amount', models.IntegerField()),
                ('contract_remarks', models.TextField()),
            ],
            options={
                'verbose_name': 'Contract',
                'verbose_name_plural': 'Contracts',
            },
        ),
        migrations.CreateModel(
            name='Main',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('total_contribution', models.IntegerField()),
            ],
            options={
                'verbose_name': 'Main',
                'verbose_name_plural': 'Mains',
            },
        ),
        migrations.CreateModel(
            name='Transaction',
            fields=[
                ('contract_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='contract.Contract')),
                ('amount', models.IntegerField()),
                ('session_month', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='Transactions', to='session.SessionMonth')),
            ],
            options={
                'verbose_name': 'Transaction',
                'verbose_name_plural': 'Transactions',
            },
            bases=('contract.contract',),
        ),
        migrations.AddField(
            model_name='contract',
            name='incharge',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='contracts', to='group.Incharge'),
        ),
        migrations.AddField(
            model_name='contract',
            name='member',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='contracts', to='member.Member'),
        ),
        migrations.AddField(
            model_name='contract',
            name='session',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='contracts', to='session.Session'),
        ),
    ]
