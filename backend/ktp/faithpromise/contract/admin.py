from django.contrib import admin
from import_export.admin import ImportExportModelAdmin
from .models import Contract, Transaction

# Register your models here.
@admin.register(Contract)
class ViewAdmin(ImportExportModelAdmin):
    pass


@admin.register(Transaction)
class ViewAdmin(ImportExportModelAdmin):
    pass

