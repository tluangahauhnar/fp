from rest_framework import serializers
from .models import Contract, Main, Transaction
from rest_framework_bulk import (
    BulkListSerializer,
    BulkSerializerMixin,
    ListBulkCreateUpdateDestroyAPIView,
)

class ContractSerializer(BulkSerializerMixin, serializers.ModelSerializer):

    class Meta:
        model = Contract
        list_serializer_class = BulkListSerializer

        fields = (
            'member',
            'session',
            'incharge',
            'start_month_contract',
            'end_month_contract',
            'monthly_contract_amount',
            'contract_remarks')


class MainSerializer(serializers.ModelSerializer):

    class Meta:
        model = Main
        fields = (
            'total_contribution'
        )




class TransactionSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = Transaction
        fields = (
            'amount',
            'session_month',
            'transaction_remarks'
            )