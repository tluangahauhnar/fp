from django.db import models

class Session(models.Model):
    """Model definition for Session."""
    name = models.CharField( max_length=50, unique=True,editable=True)
    start_month = models.DateField()
    stop_month = models.DateField()

    is_active = models.BooleanField(default=False)

    class Meta:
        """Meta definition for Session."""

        verbose_name = 'Session'
        verbose_name_plural = 'Sessions'

    def __str__(self):
        """Unicode representation of Session."""
        return self.name


MONTH_CHOICES = (
        ('July' , 'July'),
        ('August', 'August'),
        ('September', 'September'),
        ('October','October'),
        ('November','November'),
        ('December','December'),
    )

class SessionMonth(models.Model):
    """Model definition for SessionMonth."""
    
    name = models.CharField( max_length=50, unique=True,editable=True)
    month = models.CharField(max_length=50,choices=MONTH_CHOICES)
    session = models.ForeignKey(
        'Session',
         on_delete=models.CASCADE,
         related_name='sessionmonths')

    is_active = models.BooleanField(default=False)

    class Meta:
        """Meta definition for Transactionmonth."""

        verbose_name = 'SessionMonth'
        verbose_name_plural = 'SessionMonth'

    def __str__(self):
        """Unicode representation of SessionMonth."""
        return self.name