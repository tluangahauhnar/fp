from rest_framework import serializers
from .models import Session, SessionMonth


class SessionSerializer(serializers.ModelSerializer):

    class Meta:
        model = Session
        fields = (
            'name',
            'start_month',
            'stop_month',
            'is_active'
        )


class SessionMonthSerializer(serializers.ModelSerializer):

    class Meta:
        model = SessionMonth
        fields = (
            'name',
            'month',
            'session',
            'is_active',
        )
