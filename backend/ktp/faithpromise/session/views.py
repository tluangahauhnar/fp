from rest_framework import viewsets, generics

from .models import Session, SessionMonth
from .serializers import SessionSerializer, SessionMonthSerializer



class SessionListAPIView(generics.ListAPIView):
    queryset = Session.objects.all()
    serializer_class = SessionSerializer


class SessionCreateAPIView (generics.CreateAPIView):
    queryset = Session.objects.all()
    serializer_class = SessionSerializer


class SessionRetrieveUpdateAPIView(generics.RetrieveUpdateAPIView):
    queryset = Session.objects.all()
    serializer_class = SessionSerializer
    lookup_field = 'id'


class SessionRetrieveDestroyAPIView (generics.RetrieveDestroyAPIView):
    queryset = Session.objects.all()
    serializer_class = SessionSerializer
    lookup_field = 'id'


class SessionMonthRetrieveAPIView (generics.RetrieveAPIView):
    queryset = SessionMonth.objects.all()
    serializer_class = SessionMonthSerializer


class SessionMonthCreateAPIView (generics.CreateAPIView):
    queryset = SessionMonth.objects.all()
    serializer_class = SessionMonthSerializer


class SessionMonthRetrieveUpdateAPIView (generics.RetrieveUpdateAPIView):
    queryset = SessionMonth.objects.all()
    serializer_class = SessionMonthSerializer
    lookup_field = 'id'


class SessionMonthRetrieveDestroyAPIView (generics.RetrieveDestroyAPIView):
    queryset = SessionMonth.objects.all()
    serializer_class = SessionMonthSerializer
    lookup_field = 'id'