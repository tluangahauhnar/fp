from django.urls import path
from ktp.faithpromise.session.views import (
    SessionListAPIView,
    SessionCreateAPIView,
    SessionRetrieveDestroyAPIView,
    SessionRetrieveUpdateAPIView)
from ktp.faithpromise.session.views import (
    SessionMonthCreateAPIView,
    SessionMonthCreateAPIView,
    SessionMonthRetrieveUpdateAPIView,
    SessionMonthRetrieveDestroyAPIView)
from ktp.faithpromise.contract.views import (
    ContractListAPIView,
    ContractCreateAPIView,
    ContractRetrieveUpdateAPIView,
    ContractRetrieveDestroyAPIView,
    ContractListBulkCreateUpdateDestroyAPIView)


urlpatterns = [
    path('session/', SessionListAPIView.as_view(), name='session-list'),
    path('session/add/', SessionCreateAPIView.as_view(), name='session-add'),
    path('session/remove/<int:id>/',
         SessionRetrieveDestroyAPIView.as_view(), name='session-remove'),
    path('session/update/<int:id>/',
         SessionRetrieveUpdateAPIView.as_view(), name='session-update'),

    path('session/month/', SessionListAPIView.as_view(), name='month-list'),
    path('session/month/add', SessionCreateAPIView.as_view(), name='month-add'),
    path('session/month/update/<int:id>/',
         SessionMonthRetrieveUpdateAPIView.as_view(), name='month-update'),
    path('session/month/remove/<int:id>/',
         SessionRetrieveDestroyAPIView.as_view(), name='month-remove'),

    path('contract/list', ContractListAPIView.as_view(), name='contract-list'),
    path('contract/add/', ContractCreateAPIView.as_view(), name='contract-add'),
    path('contract/remove/<int:id>/', ContractRetrieveDestroyAPIView.as_view(),
         name='contract-remove'),
    path('contract/update/<int:id>/', ContractRetrieveUpdateAPIView.as_view(),
         name='contract-update'),
    path('contract/',ContractListBulkCreateUpdateDestroyAPIView.as_view(),
        name='contract-bulk')




]
