from django.contrib import admin
from .models import Group, Incharge
# Register your models here.


admin.site.register(Group)
admin.site.register(Incharge)
