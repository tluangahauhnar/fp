from django.db import models
from ktp.branch.models import KTPBranch

# Create your models here.


class Group(models.Model):
    name = models.CharField(max_length=50)
    number = models.IntegerField()
    date_of_establishment = models.DateField()
    branch = models.ForeignKey(
        'branch.KTPBranch',
        on_delete=models.CASCADE,
        related_name='group'
    )

    class Meta:
        """Meta definition for Group."""

        verbose_name = 'Group'
        verbose_name_plural = 'Groups'

    def __str__(self):
        """Unicode representation of Group."""
        return self.name

class Incharge(models.Model):
    """Model definition for CommitteeMember."""
    name = models.CharField(max_length=50)
    group = models.ForeignKey(
        'Group',
        on_delete=models.CASCADE,
        related_name='groupcommittee')
    zone_no = models.IntegerField()

    class Meta:
        """Meta definition for CommitteeMember."""

        verbose_name = 'Incharge'
        verbose_name_plural = 'Incharges'

    def __str__(self):
        """Unicode representation of CommitteeMember."""
        return self.name

class MemberDinhmun(models.Model):
    """Model definition for MemberDinhmun."""
    group = models.ForeignKey(
        'Group',
        on_delete=models.CASCADE,
        related_name='groupdinhmun')

    class Meta:
        """Meta definition for MemberDinhmun."""

        verbose_name = 'MemberDinhmun'
        verbose_name_plural = 'MemberDinhmuns'

    def __str__(self):
        """Unicode representation of MemberDinhmun."""
        pass


