from rest_framework import routers
from django.urls import path, include
from . import views

urlpatterns = [
    path('', views.GroupListAPIView.as_view(), name='group-list'),
    path('add/', views.GroupCreateAPIView.as_view(), name='add-group'),
    path('remove/<int:id>/', views.GroupDestroyAPIView.as_view(), name='remove-group'),
    path('<int:number>',views.GroupDetailAPIView.as_view(), name='group-detail'),
    path('edit/<int:number>', views.GroupEditAPIView.as_view(), name='group-edit'),
    path('update-group/', views.TestGroupEditAPIView.as_view(), name='update-group'),
]
