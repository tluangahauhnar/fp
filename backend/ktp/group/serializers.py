from rest_framework import serializers
from .models import Group, Incharge


class GroupSerializer(serializers.ModelSerializer):

    class Meta:
        model = Group
        fields = (
            'id',
            'name',
            'number',
            'date_of_establishment',
            'branch'
        )


class InchargeSerializer(serializers.ModelSerializer):

    class Meta:
        model = Incharge
        fields = (
            'name',
            'group',
            'zone_no'
        )


class JSONSerializerField(serializers.Field):
    """ Serializer for JSONField -- required to make field writable"""
    def to_internal_value(self, data):
        return data
    def to_representation(self, value):
        return value


class TestGroupSerializer(serializers.Serializer):

    update_data = serializers.JSONField()
    
    class Meta:
        fields = "__all__"