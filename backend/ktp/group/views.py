from django.shortcuts import render
import json
from django_bulk_update.manager import BulkUpdateManager
from django_bulk_update.helper import bulk_update
from rest_framework import generics, status
from .models import Group, Incharge
from .serializers import InchargeSerializer, GroupSerializer, TestGroupSerializer
from rest_framework.response import Response


# Create your views here.


class GroupListAPIView(generics.ListAPIView):
    queryset = Group.objects.all()
    serializer_class = GroupSerializer


class GroupCreateAPIView(generics.CreateAPIView):
    queryset = Group.objects.all()
    serializer_class = GroupSerializer


class GroupDestroyAPIView(generics.RetrieveDestroyAPIView):
    queryset = Group.objects.all()
    serializer_class = GroupSerializer
    lookup_field = 'id'


class GroupDetailAPIView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Group.objects.all()
    serializer_class = GroupSerializer
    lookup_field = 'number'


class GroupEditAPIView(generics.RetrieveUpdateAPIView):
    queryset = Group.objects.all()
    serializer_class = GroupSerializer
    lookup_field = 'number'


class TestGroupEditAPIView(generics.GenericAPIView):
    serializer_class = TestGroupSerializer

    def post(self, request):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        data = json.loads(request.data['update_data'])
        
        try:
            people = Group.objects.all()

            for person in people:
                for item in data:
                    print(item['id'])
                    if person.id==item['id']:
                        person.name = item['name']

            bulk_update(people, update_fields=['name','number'])

            return Response({'message': 'ok', 'success': True})
        except Exception as e:
            return Response({'message': format(e.args[-1]), 'success': False})


'''
class InchargeViewSet(viewsets.ModelViewSet):

    queryset = Incharge.objects.all()
    serializer_class = InchargeSerializer
'''
#[{"id":3,"name":"ajajul"},{"id":4,"name":"ajul"}]